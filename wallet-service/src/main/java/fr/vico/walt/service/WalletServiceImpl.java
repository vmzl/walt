package fr.vico.walt.service;

import fr.vico.walt.dao.WalletDAO;
import fr.vico.walt.entity.Asset;
import fr.vico.walt.entity.Trade;
import fr.vico.walt.entity.Wallet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

@Service
public class WalletServiceImpl implements WalletService{

    private WalletDAO walletDAO;

    @Autowired
    EnrichWalletService enrichWalletService;

    public WalletServiceImpl() {
    }

    @Autowired
    public WalletServiceImpl(WalletDAO walletDAO) {
        this.walletDAO = walletDAO;
    }

    @Override
    @Transactional
    public List<Wallet> findAll() {
        return walletDAO.findAll();
    }

    @Override
    @Transactional
    public Wallet findById(int id) {
        Wallet wallet = walletDAO.findById(id);

        return wallet;
    }

    @Override
    @Transactional
    public Wallet save(Wallet pWallet) {
        return walletDAO.save(pWallet);
    }

    @Override
    @Transactional
    public void deleteById(int id) {
        walletDAO.deleteById(id);
    }

    @Override
    public Wallet updateWalletValue(Wallet pWallet) {
        //TODO : check le price seulement si lastUpdated supérieur à un certain temps
        if(pWallet!=null && pWallet.getAssets()!=null & !pWallet.getAssets().isEmpty()){
            enrichWalletService.updateWallet(pWallet);
            save(pWallet);
        }
        return pWallet;
    }


    @Override
    public Asset getAsset(int id, String assetSymbol) {
        Wallet wallet = findById(id);
        return wallet.getAsset(assetSymbol);
    }

    @Override
    @Transactional
    public Wallet addAsset(int id, Asset pAsset) {
        Wallet wallet = findById(id);
        wallet.addAsset(pAsset);
        return save(wallet);
    }

    @Override
    @Transactional
    public Wallet removeAsset(int id, String assetSymbol) {
        Wallet wallet = findById(id);
        wallet.removeAsset(assetSymbol);
        return save(wallet);
    }

    @Override
    @Transactional
    public Trade getTrade(int walletId, String assetSymbol, int tradeId) {
        return findById(walletId).getAsset(assetSymbol).getTrade(tradeId);
    }

    @Override
    @Transactional
    public Wallet openTrade(int id, String symbol, Trade pTrade) {
        Wallet wallet = findById(id);
        Asset asset = wallet.getAsset(symbol);
        System.out.println(pTrade);

        if(pTrade.getQuantity()>asset.getQuantityAvailable()){
            throw new RuntimeException("Asset has not enough quantity available to create this trade [have :" + asset.getQuantityAvailable() + ", ask :" + pTrade.getQuantity() + "]");
        }else {
            asset.addTrade(pTrade);
            asset.setQuantityAvailable(asset.getQuantityAvailable()-pTrade.getQuantity());
            System.out.println(wallet);
            return save(wallet);
        }
    }

    @Override
    @Transactional
    public Wallet closeTrade(int id, String symbol, Trade pTrade) {
        //TODO : a terme, possibilité de n'en fermer qu'une partie (du trade). Il faudra alors créé un nouveau trade pour la partie fermée, et actualiser celui-ci avec la partie restant ouverte
        Wallet wallet = findById(id);

        Asset asset = wallet.getAsset(symbol);
        asset = enrichWalletService.updateAssetPrice(asset);

        Trade trade = asset.getTrade(pTrade.getId());
        trade.setPriceClosed(asset.getPrice());
        trade.setClosedAt(new Timestamp(System.currentTimeMillis()));
        trade.setProfit((trade.getPriceClosed()-trade.getPriceOpened())*trade.getQuantity());
        asset.setQuantityAvailable(asset.getQuantityAvailable()+trade.getQuantity());

        return save(wallet);
    }


}
