package fr.vico.walt.service;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import fr.vico.walt.entity.Asset;
import fr.vico.walt.entity.Wallet;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.sql.Timestamp;

@Service
public class EnrichWalletService {

    private final RestTemplate restTemplate;

    public EnrichWalletService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @Value("${walt.service.crypto.price.url}")
    private String urlWaltCryptoPriceService;


    public void updateWallet(Wallet pWallet) {

        for(Asset asset : pWallet.getAssets()){

            updateAssetPrice(asset);

        }
        Double total = 0.0;
        for(Asset asset : pWallet.getAssets()){
            total+=asset.getPrice()*asset.getQuantityTotal();
        }
        pWallet.setTotalValue(total);
        pWallet.setLastUpdated(new Timestamp(System.currentTimeMillis()));
    }

    public Asset updateAssetPrice(Asset asset) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

        UriComponentsBuilder builder;

        HttpEntity<?> entity;
        HttpEntity<String> response;

        builder = UriComponentsBuilder.fromHttpUrl(urlWaltCryptoPriceService + asset.getAssetSymbol());

        entity = new HttpEntity<>(headers);

        //TODO : check status reponse bien entre 200 et 300, sinon erreur (cas d'un symbol inextistant)
        response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                String.class);

        //Get price from the response
        Gson gson = new Gson();
        String price = gson.fromJson (response.getBody(), JsonElement.class).getAsString();
        asset.setPrice(Double.valueOf(price));
        asset.setLastUpdated(new Timestamp(System.currentTimeMillis()));

        return asset;
    }
}
