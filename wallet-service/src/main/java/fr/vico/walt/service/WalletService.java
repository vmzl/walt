package fr.vico.walt.service;

import fr.vico.walt.entity.Asset;
import fr.vico.walt.entity.Trade;
import fr.vico.walt.entity.Wallet;

import java.util.List;

public interface WalletService {
    public List<Wallet> findAll();

    public Wallet findById(int id);

    public Wallet save(Wallet pWallet);

    public void deleteById(int id);

    public Wallet updateWalletValue(Wallet pWallet);

    public Asset getAsset(int id, String assetSymbol);

    public Wallet addAsset(int id, Asset pAsset);

    public Wallet removeAsset(int id, String assetSymbol);

    public Trade getTrade(int walletId, String assetSymbol, int tradeId);

    public Wallet openTrade(int id, String symbol, Trade pTrade);

    public Wallet closeTrade(int id, String symbol, Trade pTrade);
}
