package fr.vico.walt.exception;

public class NonExistingWalletException extends Exception{
    public NonExistingWalletException(String message){
        super(message);
    }
}
