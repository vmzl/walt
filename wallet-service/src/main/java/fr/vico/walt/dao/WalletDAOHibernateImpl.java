package fr.vico.walt.dao;

import fr.vico.walt.entity.Wallet;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class WalletDAOHibernateImpl implements WalletDAO{

    private EntityManager entityManager;

    @Autowired
    public WalletDAOHibernateImpl(EntityManager pEntityManager){
        entityManager = pEntityManager;
    }


    @Override
    public List<Wallet> findAll() {
        //get the hibernate session
        Session currentSession = entityManager.unwrap(Session.class);

        //create and execute the query
        Query<Wallet> theQuery =
                currentSession.createQuery("from Wallet", Wallet.class);
        List<Wallet> wallets = theQuery.getResultList();

        //return the result
        return wallets;
    }

    @Override
    public Wallet findById(int id) {
        Session currentSession = entityManager.unwrap(Session.class);

        Wallet wallet =
                currentSession.get(Wallet.class, id);

        return wallet;
    }

    @Override
    public Wallet save(Wallet pWallet) {

        Session currentSession = entityManager.unwrap(Session.class);

        //save/insert si id=0, update sinon
        currentSession.saveOrUpdate(pWallet);
        return pWallet;
    }

    @Override
    public void deleteById(int id) {
        Session currentSession = entityManager.unwrap(Session.class);

        Query query =
                currentSession.createQuery("delete from Wallet where id=:id");
        query.setParameter("id", id);
        query.executeUpdate();
    }
}
