package fr.vico.walt.dao;

import fr.vico.walt.entity.Wallet;

import java.util.List;

public interface WalletDAO {

    public List<Wallet> findAll();

    public Wallet findById(int id);

    public Wallet save(Wallet pWallet);

    public void deleteById(int id);
}
