package fr.vico.walt.rest;

import fr.vico.walt.entity.Asset;
import fr.vico.walt.entity.Trade;
import fr.vico.walt.entity.Wallet;
import fr.vico.walt.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/")
public class TradeRestController {

    private WalletService walletService;

    @Autowired
    public TradeRestController(WalletService pWalletService){
        walletService = pWalletService;
    }

    @PostMapping("/wallet/{walletId}/{assetSymbol}")
    public Wallet openTrade(@PathVariable int walletId, @PathVariable String assetSymbol, @RequestBody Trade pTrade){
        return walletService.openTrade(walletId, assetSymbol, pTrade);
    }

    @PutMapping("/wallet/{walletId}/{assetSymbol}")
    public Wallet closeTrade(@PathVariable int walletId, @PathVariable String assetSymbol, @RequestBody Trade pTrade){
        return walletService.closeTrade(walletId, assetSymbol, pTrade);
    }
}
