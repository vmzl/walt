package fr.vico.walt.rest;

import fr.vico.walt.entity.Asset;
import fr.vico.walt.entity.Wallet;
import fr.vico.walt.service.EnrichWalletService;
import fr.vico.walt.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/")
public class AssetRestController {

    private WalletService walletService;

    @Autowired
    public AssetRestController(WalletService pWalletService){
        walletService = pWalletService;
    }

    @GetMapping("/wallet/{walletId}/{assetSymbol}")
    public Asset getAsset(@PathVariable int walletId, @PathVariable String assetSymbol){
        return walletService.getAsset(walletId, assetSymbol);
    }

    @PostMapping("/wallet/{id}")
    public Wallet addAsset(@PathVariable int id, @RequestBody Asset asset){
        return walletService.addAsset(id, asset);
    }

    @DeleteMapping("/wallet/{walletId}/{assetSymbol}")
    public Wallet removeAsset(@PathVariable int walletId, @PathVariable String assetSymbol){
        return walletService.removeAsset(walletId, assetSymbol);
    }
}
