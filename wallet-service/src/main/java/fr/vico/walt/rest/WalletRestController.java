package fr.vico.walt.rest;

import fr.vico.walt.entity.Asset;
import fr.vico.walt.entity.Wallet;
import fr.vico.walt.exception.NonExistingWalletException;
import fr.vico.walt.service.EnrichWalletService;
import fr.vico.walt.service.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("api/")
public class WalletRestController {

    private WalletService walletService;

    @Autowired
    public WalletRestController(WalletService pWalletService){
        walletService = pWalletService;
    }


    @GetMapping("/wallet")
    public List<Wallet> findAll(){
        return walletService.findAll();
    }

    @GetMapping("/wallet/{id}")
    public Wallet getWallet(@PathVariable int id) throws NonExistingWalletException {
        Wallet wallet = walletService.findById(id);
        walletService.updateWalletValue(wallet);

        if(wallet == null){
            throw new NonExistingWalletException("Wallet id not found : " + id);
        }

        return wallet;
    }

    @PostMapping("/wallet")
    public Wallet addWallet(@RequestBody Wallet pWallet){

        return walletService.save(pWallet);
    }

    @PutMapping("/wallet")
    public Wallet updateWallet(@RequestBody Wallet pWallet){
        return walletService.save(pWallet);
    }

    @DeleteMapping("/wallet/{id}")
    public String deleteWallet(@PathVariable int id){
        Wallet tWallet = walletService.findById(id);

        if(tWallet == null){
            throw new RuntimeException("Wallet id not found : " + id);
        }

        walletService.deleteById(id);
        return "Deleted Wallet with id : " + id;
    }
}
