package fr.vico.walt.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name="trade")
public class Trade {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Column(name="quantity")
    private Double quantity;

    @Column(name="price_opened")
    private Double priceOpened;

    @Column(name="opened_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date openedAt;

    @Column(name="price_closed")
    private Double priceClosed;

    @Column(name="closed_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date closedAt;

    @Column(name="profit")
    private Double profit;

    public Trade() {
        openedAt = new Timestamp(System.currentTimeMillis());
    }

    public Trade(int id, double quantity, double priceOpened, Date openedAt, double priceClosed, Date closedAt, double profit) {
        this.id = id;
        this.quantity = quantity;
        this.priceOpened = priceOpened;
        this.openedAt = openedAt;
        this.priceClosed = priceClosed;
        this.closedAt = closedAt;
        this.profit = profit;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getPriceOpened() {
        return priceOpened;
    }

    public void setPriceOpened(Double priceOpened) {
        this.priceOpened = priceOpened;
    }

    public Date getOpenedAt() {
        return openedAt;
    }

    public void setOpenedAt(Date openedAt) {
        this.openedAt = openedAt;
    }

    public Double getPriceClosed() {
        return priceClosed;
    }

    public void setPriceClosed(Double priceClosed) {
        this.priceClosed = priceClosed;
    }

    public Date getClosedAt() {
        return closedAt;
    }

    public void setClosedAt(Date closedAt) {
        this.closedAt = closedAt;
    }

    public Double getProfit() {
        return profit;
    }

    public void setProfit(Double profit) {
        this.profit = profit;
    }

    @Override
    public String toString() {
        return "Trade{" +
                "id=" + id +
                ", quantity=" + quantity +
                ", priceOpened=" + priceOpened +
                ", openedAt=" + openedAt +
                ", priceClosed=" + priceClosed +
                ", closedAt=" + closedAt +
                ", profit=" + profit +
                '}';
    }
}
