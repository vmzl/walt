package fr.vico.walt.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="asset")
public class Asset {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Column(name="asset_symbol")
    private String assetSymbol;

    @Column(name="quantity_total")
    private Double quantityTotal;

    @Column(name="price")
    private Double price;

    @Column(name="quantity_available")
    private Double quantityAvailable;

    @Column(name = "last_updated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;

    @OneToMany(fetch=FetchType.LAZY,
            cascade = CascadeType.ALL)
    @JoinColumn(name="asset_id")
    private List<Trade> trades;

    public Asset() {
        this.lastUpdated = new Timestamp(System.currentTimeMillis());
    }

    public Asset(Integer id, String assetSymbol, Double quantity, Double price, Date lastUpdated, List<Trade> trades) {
        this.id = id;
        this.assetSymbol = assetSymbol;
        this.quantityTotal = quantity;
        this.price = price;
        this.quantityAvailable = quantity;
        this.lastUpdated = lastUpdated;
        this.trades = trades;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAssetSymbol() {
        return assetSymbol;
    }

    public void setAssetSymbol(String assetSymbol) {
        this.assetSymbol = assetSymbol;
    }

    public Double getQuantityTotal() {
        return quantityTotal;
    }

    public void setQuantityTotal(Double quantityTotal) {
        this.quantityTotal = quantityTotal;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getQuantityAvailable() {
        return quantityAvailable;
    }

    public void setQuantityAvailable(Double quantityAvailable) {
        this.quantityAvailable = quantityAvailable;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public List<Trade> getTrades() {
        return trades;
    }

    public void setTrades(List<Trade> trades) {
        this.trades = trades;
    }

    public Trade getTrade(Integer id){
        Trade foundTrade = null;
        for(Trade tTrade : getTrades()){
            if(tTrade.getId() == id){
                foundTrade = tTrade;
                break;
            }
        }
        if(foundTrade==null){
            throw new RuntimeException("Asset with symbol: "+getAssetSymbol()+" does not have the trade :" + id);
        }else{
            return foundTrade;
        }
    }

    public void addTrade(Trade pTrade){
        if(getTrades()==null){
            setTrades(new ArrayList<Trade>());
        }
        this.trades.add(pTrade);
    }

    public void removeTrade(Trade pTrade){
        Trade foundTrade = null;
        for(Trade tTrade : getTrades()){
            if(pTrade.getId() == tTrade.getId()){
                foundTrade = tTrade;
                break;
            }
        }
        if(foundTrade==null){
            throw new RuntimeException("Asset with symbol:"+getAssetSymbol()+" does not have the trade :" + pTrade.getId());
        }else{
            this.trades.remove(foundTrade);
        }
    }

    @Override
    public String toString() {
        return "Asset{" +
                "id=" + id +
                ", assetSymbol='" + assetSymbol + '\'' +
                ", quantityTotal=" + quantityTotal +
                ", price=" + price +
                ", quantityAvailable=" + quantityAvailable +
                ", lastUpdated=" + lastUpdated +
                ", trades=" + trades +
                '}';
    }
}
