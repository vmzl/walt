package fr.vico.walt.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "wallet")
public class Wallet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "total_value")
    private Double totalValue;

    @Column(name = "last_updated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;

    @OneToMany(fetch=FetchType.LAZY,
            cascade = CascadeType.ALL)
    @JoinColumn(name="wallet_id")
    private List<Asset> assets;

    public Wallet() {
        this.lastUpdated = new Timestamp(System.currentTimeMillis());
    }

    public Wallet(Integer id, Double totalValue, Date lastUpdated, List<Asset> assets) {
        this.id = id;
        this.totalValue = totalValue;
        this.lastUpdated = lastUpdated;
        this.assets = assets;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(Double totalValue) {
        this.totalValue = totalValue;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public List<Asset> getAssets() {
        return assets;
    }

    public void setAssets(List<Asset> assets) {
        this.assets = assets;
    }

    public Asset getAsset(String symbol){
        Asset foundAsset = null;
        for(Asset tAsset : getAssets()){
            if(tAsset.getAssetSymbol().equals(symbol)){
                foundAsset = tAsset;
                break;
            }
        }
        if(foundAsset==null){
            throw new RuntimeException("Wallet with id: "+getId()+" does not have the asset :" + symbol);
        }else{
            return foundAsset;
        }
    }

    public void addAsset(Asset pAsset){
        if(getAssets()==null){
            setAssets(new ArrayList<Asset>());
        }
        this.assets.add(pAsset);
    }

    public void removeAsset(String assetSymbol){
        Asset foundAsset = null;
        for(Asset tAsset : getAssets()){
            if(assetSymbol.equals(tAsset.getAssetSymbol())){
                foundAsset = tAsset;
                break;
            }
        }
        if(foundAsset==null){
            throw new RuntimeException("Wallet with id:"+getId()+" does not have the asset :" + assetSymbol);
        }else{
            this.assets.remove(foundAsset);
        }
    }

    @Override
    public String toString() {
        return "Wallet{" +
                "id=" + id +
                ", totalValue=" + totalValue +
                ", lastUpdated=" + lastUpdated +
                ", assets=" + assets +
                '}';
    }
}
