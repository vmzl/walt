DROP TABLE IF EXISTS `trade`;
DROP TABLE IF EXISTS `asset`;
DROP TABLE IF EXISTS `wallet`;

CREATE TABLE `wallet` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `total_value` double DEFAULT NULL,
  `last_updated` TIMESTAMP
) ;

CREATE TABLE `asset` (
     `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
     `wallet_id` int DEFAULT NULL,
     `asset_symbol` varchar(20) NOT NULL,
     `quantity_total` double NOT NULL,
     `price` double NOT NULL,
     `quantity_available` double NOT NULL,
     `last_updated` TIMESTAMP,
     FOREIGN KEY (wallet_id) REFERENCES wallet(id)
);

CREATE TABLE `trade` (
     `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
     `asset_id` int DEFAULT NULL,
     `quantity` double NOT NULL,
     `price_opened` double NOT NULL,
     `opened_at` TIMESTAMP,
     `price_closed` double,
     `closed_at` TIMESTAMP,
     `profit` double,
     FOREIGN KEY (asset_id) REFERENCES asset(id)
);

INSERT INTO `wallet` VALUES
    (1, NULL, NOW());

INSERT INTO `asset` VALUES
    (1, 1, 'BTC', 1.0, 0.0, 1.0, NOW()),
    (2, 1, 'ETH', 1.0, 0.0, 1.0, NOW());

INSERT INTO `trade` VALUES
    (1, 1, 1.0, 10000, NOW(), NULL, NULL, NULL);