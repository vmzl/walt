package fr.vico.walt;

import fr.vico.walt.dao.WalletDAO;
import fr.vico.walt.entity.Wallet;
import fr.vico.walt.exception.NonExistingWalletException;
import fr.vico.walt.service.WalletService;
import fr.vico.walt.service.WalletServiceImpl;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class WalletCRUDTests {

    @InjectMocks //@InjectMocks = service à tester
    WalletServiceImpl walletService;

    @Mock //@Mock les dépendances du service à tester
    WalletDAO dao;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void shouldFindAllWallets()
    {
        List<Wallet> list = new ArrayList<Wallet>();
        Wallet wOne = new Wallet(Integer.valueOf(1), 1000.0, new Timestamp(System.currentTimeMillis()), null);
        Wallet wTwo = new Wallet(Integer.valueOf(2), 2000.0, new Timestamp(System.currentTimeMillis()), null);
        Wallet wThree = new Wallet(Integer.valueOf(3), 3000.0, new Timestamp(System.currentTimeMillis()), null);

        list.add(wOne);
        list.add(wTwo);
        list.add(wThree);

        when(dao.findAll()).thenReturn(list);

        //test
        List<Wallet> wList = walletService.findAll();

        assertEquals(list, wList);
    }

    @Test
    public void whenGETWallet_shouldReturnWallet() {
        Wallet wOne = new Wallet(Integer.valueOf(1), 1000.0, new Timestamp(System.currentTimeMillis()), null);

        when(dao.findById(wOne.getId())).thenReturn(wOne);

        Wallet found =  walletService.findById(wOne.getId());

        assertEquals(wOne, found);
    }

    @Test
    public void whenGETNonExistingWallet_shouldReturnNull(){
        Integer id = 9999;
        when(dao.findById(id)).thenReturn(null);

        assertNull(walletService.findById(id));
    }

    @Test
    public void whenAddNewWallet_shouldReturnTheWallet(){
        Wallet wOne = new Wallet(Integer.valueOf(1), 1000.0, new Timestamp(System.currentTimeMillis()), null);

        doReturn(wOne).when(dao).save(wOne);

        Wallet addedWallet = walletService.save(wOne);

        assertEquals(wOne, addedWallet);
    }

    @Test
    public void whenUpdateWallet_shouldReturnTheUpdatedWallet(){
        Wallet wOne = new Wallet(Integer.valueOf(1), 1000.0, new Timestamp(System.currentTimeMillis()), null);

        //doReturn(wOne).when(dao).save(wOne);
        when(dao.save(wOne)).thenReturn(wOne);

        Wallet updatedWallet = walletService.save(wOne);

        assertEquals(wOne, updatedWallet);
    }

    @Test
    public void whenDELETEWallet_shouldCallDeleteMethod(){
        Integer deleteId = 1;

        dao.deleteById(deleteId);

        verify(dao, times(1)).deleteById(deleteId);

    }
}
