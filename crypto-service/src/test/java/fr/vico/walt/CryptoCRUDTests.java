package fr.vico.walt;

import fr.vico.walt.entity.Crypto;
import fr.vico.walt.exception.NonExistingCryptoException;
import fr.vico.walt.rest.CryptoRestController;
import fr.vico.walt.service.CryptoService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
public class CryptoCRUDTests {


    @Autowired
    private CryptoService cryptoService;

    @Autowired
    private CryptoRestController cryptorestController;

    @Value("${crypto.delete.validateMessage}")
    private String validateDeleteMessage;

    @Test
    public void whenGETCrypto_shouldReturnCrypto(){
        String symbol = "BTC";
        Crypto found = null;
        try {
            found = cryptorestController.getCrypto(symbol);
        } catch (NonExistingCryptoException e) {
            e.printStackTrace();
        }

        assertEquals(found.getSymbol(), symbol);
    }

    @Test
    public void whenGETNonExistingCrypto_shouldReturnException(){
        String symbol = "NonExistingSymbol";

        assertThrows(NonExistingCryptoException.class, () -> {
            cryptorestController.getCrypto(symbol);
        });
    }

    @Test
    public void whenPOSTNewCrypto_shouldReturnTheCrypto(){
        Crypto newCrypto = new Crypto("NEW", "New Token", 0, new Timestamp(System.currentTimeMillis()));

        Crypto addedCrypto = cryptorestController.addCrypto(newCrypto);

        assertEquals(newCrypto, addedCrypto);
    }

    @Test
    public void whenPUTCrypto_shouldReturnTheUpdatedCrypto(){
        Crypto updateCrypto = new Crypto("BTC", "Bitcoin", 42, new Timestamp(System.currentTimeMillis()));

        Crypto updatedCrypto = cryptorestController.addCrypto(updateCrypto);

        assertEquals(updateCrypto, updatedCrypto);
    }

    @Test
    public void whenDELETECrypto_shouldReturnValidationString(){
        String symbol = "BTC";

        try {
            assertEquals(cryptorestController.deleteCrypto(symbol), validateDeleteMessage + symbol);
        } catch (NonExistingCryptoException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void whenGETNonExistingCryptoPrice_shouldReturnError(){
        String symbol = "NonExistingSymbol";

        assertThrows(NonExistingCryptoException.class, () -> {
            cryptorestController.getCryptoPrice(symbol);
        });
    }

    @Test
    public void whenGETCryptoPrice_shouldReturnUpdatedCryptoPrice(){
        String symbol = "BTC";
        Float price = 0F; //Dans notre jeux de données test, le prix du BTC est de 0, il doit être mis à jour.
        try {
            price = cryptorestController.getCryptoPrice(symbol);
        } catch (NonExistingCryptoException e) {
            e.printStackTrace();
        }

        assertNotEquals(0F, price);
    }

}
