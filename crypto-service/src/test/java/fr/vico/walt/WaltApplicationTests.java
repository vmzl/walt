package fr.vico.walt;

import fr.vico.walt.entity.Crypto;
import fr.vico.walt.exception.NonExistingCryptoException;
import fr.vico.walt.rest.CryptoRestController;
import fr.vico.walt.service.CryptoService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.Assert.*;


@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
class WaltApplicationTests {


	@Autowired
	private CryptoService cryptoService;


	@Test
	public void whenValidName_thenCryptoShouldBeFound() {
		/*Crypto crypto = new Crypto("BTC", "Bitcoin", 11000, new Timestamp(System.currentTimeMillis()));

		Mockito.when(cryptoDAO.findById(crypto.getSymbol()))
				.thenReturn(crypto);*/

		//--------------

		String symbol = "BTC";
		Crypto found = cryptoService.findById(symbol);
		System.out.println(found);

		assertEquals(found.getSymbol(), symbol);
	}


}
