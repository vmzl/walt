DROP TABLE IF EXISTS `crypto`;

CREATE TABLE `crypto` (
   `symbol` varchar(20) PRIMARY KEY NOT NULL,
  `name` varchar(20) NOT NULL,
  `price` float,
  `last_updated` TIMESTAMP
) ;

INSERT INTO `crypto` VALUES
    ('BTC', 'Bitcoin', 10000, NOW()),
    ('ETH', 'Etherium', 1, NOW()),
    ('ETC', 'Ethrium Classic', 0, NOW()),
    ('BTG', 'Bitcoin Gold', 0, NOW()),
    ('DOGE', 'DogeCoin', 0, NOW())