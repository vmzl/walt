package fr.vico.walt.dao;

import fr.vico.walt.entity.Crypto;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class CryptoDAOHibernateImpl implements CryptoDAO {

    private EntityManager entityManager;

    @Autowired
    public CryptoDAOHibernateImpl(EntityManager pEntityManager){
        entityManager = pEntityManager;
    }


    @Override
    public List<Crypto> findAll() {

        //get the hibernate session
       Session currentSession = entityManager.unwrap(Session.class);

       //create and execute the query
        Query<Crypto> theQuery =
                currentSession.createQuery("from Crypto", Crypto.class);
        List<Crypto> cryptos = theQuery.getResultList();

        //return the result
        return cryptos;
    }

    @Override
    public Crypto findById(String pSymbol) {
        Session currentSession = entityManager.unwrap(Session.class);

        Crypto crypto =
                currentSession.get(Crypto.class, pSymbol);

        return crypto;
    }


    @Override
    public Crypto save(Crypto pCrypto) {
        Session currentSession = entityManager.unwrap(Session.class);

        //save/insert si id=0, update sinon
        currentSession.saveOrUpdate(pCrypto);
        return pCrypto;
    }

    @Override
    public void deleteById(String pSymbol) {
        Session currentSession = entityManager.unwrap(Session.class);

        Query query =
                currentSession.createQuery("delete from Crypto where symbol=:symbol");
        query.setParameter("symbol", pSymbol);
        query.executeUpdate();
    }


}
