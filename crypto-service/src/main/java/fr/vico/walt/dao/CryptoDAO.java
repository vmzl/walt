package fr.vico.walt.dao;

import fr.vico.walt.entity.Crypto;

import java.util.List;

public interface CryptoDAO {

    public List<Crypto> findAll();

    public Crypto findById(String pSymbol);

    public Crypto save(Crypto pCrypto);

    public void deleteById(String pSymbol);
}
