package fr.vico.walt.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "crypto")
public class Crypto {
    @Id
    @Column(name = "symbol")
    private String symbol;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private float price;

    @Column(name = "last_updated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated;

    //default constructor is automatically used. We can set default values inside
    //default values will be automatically replaced if furnished
    public Crypto() {
        this.price = 0;
        this.lastUpdated = new Timestamp(System.currentTimeMillis());
    }

    public Crypto(String name, float price, Date lastUpdated) {
        this.name = name;
        this.price = price;
        this.lastUpdated = lastUpdated;
    }

    public Crypto(String symbol, String name, float price, Date lastUpdated) {
        this.symbol = symbol;
        this.name = name;
        this.price = price;
        this.lastUpdated = lastUpdated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setShortName(String symbol) {
        this.symbol = symbol;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public String toString() {
        return "Crypto{" +
                "symbol='" + symbol + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", lastUpdated=" + lastUpdated +
                '}';
    }
}
