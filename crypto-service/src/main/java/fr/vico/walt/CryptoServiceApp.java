package fr.vico.walt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CryptoServiceApp {

	public static void main(String[] args) {
		SpringApplication.run(CryptoServiceApp.class, args);
	}

}
