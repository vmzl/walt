package fr.vico.walt;

import fr.vico.walt.service.EnrichService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @Value("${team.creator}")
    private String creator;
    @Value("${team.project}")
    private String project;

    @Autowired
    EnrichService rs;

    @GetMapping("/")
    public String sayWelcome() {
        return "Welcome to " + project + ", dear " + creator + " !";
    }

    @GetMapping("/test")
    public String sayTheTest() {
        return "Nothing on test";
    }
}
