package fr.vico.walt.service;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class EnrichService {

    @Value("${enrich.coinmarketcap.url}")
    private String urlCoinMarketCap;

    @Value("${enrich.coinmarketcap.api.key}")
    private String coinMarketCapAPIKey;

    @Value("${enrich.coinmarketcap.api.value}")
    private String coinMarketCapAPIValue;

    @Value("${enrich.coinmarketcap.api.symbol}")
    private String coinMarketCapAPISymbol;

    private final RestTemplate restTemplate;

    public EnrichService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public String getCryptoPriceUSD(String symbol) {

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlCoinMarketCap)
                .queryParam(coinMarketCapAPIKey, coinMarketCapAPIValue)
                .queryParam(coinMarketCapAPISymbol, symbol);

        HttpEntity<?> entity = new HttpEntity<>(headers);

        //TODO : check status reponse bien entre 200 et 300, sinon erreur (cas d'un symbol inextistant)
        HttpEntity<String> response = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                entity,
                String.class);


        //Get price from the response
        Gson gson = new Gson();
        String price = gson.fromJson (response.getBody(), JsonElement.class).getAsJsonObject().getAsJsonObject("data").getAsJsonObject(symbol).getAsJsonObject("quote").
                getAsJsonObject("USD").
                get("price").getAsString();

        return price;
    }
}
