package fr.vico.walt.service;

import fr.vico.walt.entity.Crypto;

import java.util.List;

public interface CryptoService {

    public List<Crypto> findAll();

    public Crypto findById(String symbol);

    public Crypto save(Crypto pCrypto);

    public void deleteById(String symbol);

    public Crypto updatePrice(Crypto crypto);
}
