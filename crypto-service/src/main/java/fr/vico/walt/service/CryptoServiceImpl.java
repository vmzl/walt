package fr.vico.walt.service;

import fr.vico.walt.dao.CryptoDAO;
import fr.vico.walt.entity.Crypto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

@Service
public class CryptoServiceImpl implements CryptoService{

    private EnrichService enrichService;
    private CryptoDAO cryptoDAO;

    public CryptoServiceImpl() {
    }

    @Autowired
    public CryptoServiceImpl(CryptoDAO cryptoDAO, EnrichService enrichService) {
        this.cryptoDAO = cryptoDAO;
        this.enrichService = enrichService;
    }

    @Override
    @Transactional
    public List<Crypto> findAll() {
        return cryptoDAO.findAll();
    }

    @Override
    @Transactional
    public Crypto findById(String symbol) {
        Crypto crypto = cryptoDAO.findById(symbol);

        return crypto;
    }

    @Override
    @Transactional
    public Crypto save(Crypto pCrypto) {
        return cryptoDAO.save(pCrypto);
    }

    @Override
    @Transactional
    public void deleteById(String symbol) {
        cryptoDAO.deleteById(symbol);
    }

    @Override
    @Transactional
    public Crypto updatePrice(Crypto pCrypto) {
        //TODO : check le price seulement si lastUpdated supérieur à un certain temps
        pCrypto.setPrice(Float.parseFloat(enrichService.getCryptoPriceUSD(pCrypto.getSymbol())));
        pCrypto.setLastUpdated(new Timestamp(System.currentTimeMillis()));
        return save(pCrypto);
    }
}
