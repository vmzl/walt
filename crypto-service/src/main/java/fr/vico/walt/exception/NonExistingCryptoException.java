package fr.vico.walt.exception;

public class NonExistingCryptoException extends Exception{
    public NonExistingCryptoException(String message){
        super(message);
    }
}
