package fr.vico.walt.rest;

import fr.vico.walt.entity.Crypto;
import fr.vico.walt.exception.NonExistingCryptoException;
import fr.vico.walt.service.CryptoService;
import fr.vico.walt.service.EnrichService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api")
public class CryptoRestController {

    private CryptoService cryptoService;

    @Value("${crypto.delete.validateMessage}")
    private String validateDeleteMessage;

    @Autowired
    EnrichService rs;

    @Autowired
    public CryptoRestController(CryptoService pCryptoService){
        cryptoService = pCryptoService;
    }

    //expose /crypto and return list of cryptos
    @GetMapping("/crypto")
    public List<Crypto> findAll(){
        return cryptoService.findAll();
    }

    @GetMapping("/crypto/{symbol}")
    public Crypto getCrypto(@PathVariable String symbol) throws NonExistingCryptoException {
        Crypto crypto = cryptoService.findById(symbol);

        if(crypto == null){
            throw new NonExistingCryptoException("Symbol [" + symbol + "] not found");
        }

        if(crypto!=null) {
            cryptoService.updatePrice(crypto);
        }

        return crypto;
    }

    @GetMapping("/crypto/price/{symbol}")
    public Float getCryptoPrice(@PathVariable String symbol) throws NonExistingCryptoException {
        Crypto crypto = getCrypto(symbol);
        return crypto.getPrice();
    }

    @PostMapping("/crypto")
    public Crypto addCrypto(@RequestBody Crypto pCrypto){

        cryptoService.save(pCrypto);

        return pCrypto;
    }

    @PutMapping("/crypto")
    public Crypto updateCrypto(@RequestBody Crypto pCrypto){
        cryptoService.save(pCrypto);

        return pCrypto;
    }

    @DeleteMapping("/crypto/{symbol}")
    public String deleteCrypto(@PathVariable String symbol) throws NonExistingCryptoException {
        Crypto tCrypto = cryptoService.findById(symbol);

        if(tCrypto == null){
            throw new NonExistingCryptoException("Symbol [" + symbol + "] not found");
        }

        cryptoService.deleteById(symbol);
        return validateDeleteMessage + symbol;
    }


}
