# Walt


Walt est un gestionnaire de portefeuille de cryptomonnaies. C'est un projet personnel effectué sur mon temps libre.

Ce projet est le backend de Walt. C'est une architecture en microservices exposant une API REST.\
 Il utilise les technologies suivantes :

* [Spring Boot](http://projects.spring.io/spring-boot/)
* [Hibernate](https://hibernate.org/)
* [MySQL](https://www.mysql.com/)
* [CoinMarketCap API](https://coinmarketcap.com/api/documentation/v1/)

# Features
### Services
Le projet est séparé en deux services.
#### **crypto-service** 
Contient l'ensemble des cryptomonnaies utilisées par Walt. \
Utilise l'API CoinMarketCap pour mettre à jour les prix des cryptomonnaies. \
La mise à jour du prix s'effectue lors d'un GET sur une monnaie.


###### Model
 `Crypto` :
```java
public class Crypto {
    private String symbol;
    private String name;
    private float price;
    private Date lastUpdated;
}
```

#### **wallet-service**
Contient l'aspect gestion des wallets.\
Chaque `Wallet` peut avoir des `Asset`, chaque `Asset` peut avoir des `Trade`(ouvert ou fermé).\
Utilise le service **crypto-service** pour maintenir les `Asset` à jour.\
L'ouverture d'un `Trade` est conditionné par la disponibilité de l'`Asset`.\
La fermeture d'un `Trade` engendre un profit (positif ou négatif).

######Model 
`Wallet` :
```java
public class Wallet {
    private Integer id;
    private Double totalValue;
    private String name;
    private Date lastUpdated;
    private List<Asset> assets;
}
```

 `Asset` :
```java
public class Asset {
    private Integer id;
    private String symbol;
    private String name;
    private Double quantityTotal;
    private Double price;
    private Double quantityAvailable;
    private Date lastUpdated;
    private List<Trade> trades;
}
```

 `Trade` :
```java
public class Trade {
    private Integer id;
    private Double quantity;
    private Double priceOpened;
    private Date openedAt;
    private Double priceClosed;
    private Date closedAt;
    private Double profit;
}
```


### Endpoints

Les endpoints disponibles (_/walt/api_ doit précéder l'URL).

###### crypto-service

Fonction | URL | Requête | Query Param | Body Param | Retour
-------- | --- | ------- | ----------- | ---------- | ------
Get ALL cryptos | _/crypto_ | GET | - | - | `List<Crypto>`
Get a crypto | _/crypto/{symbol}_ | GET | _symbol_ de la crypto | - | `Crypto`
Get a price crypto | _/crypto/price/{symbol}_ | GET | _symbol_ de la crypto | - | `Float price`
Create a crypto | _/crypto_ | POST | - | `Crypto` | `Crypto`
Update a crypto | _/crypto_ | PUT | - | `Crypto` | `Crypto`
Delete a crypto | _/crypto_ | DELETE | - | `Crypto` | -

###### wallet-service

Fonction | URL | Requête | Query Param | Body Param | Retour
-------- | --- | ------- | ----------- | ---------- | ------
Get ALL wallets | _/wallet_ | GET | - | - | `List<Wallet>`
Get a wallet | _/wallet/{id}_ | GET | _id_ du wallet | - | `Wallet` 
Create a wallet | _/wallet_ | POST | - | `Wallet` | `Wallet`
Update a wallet | _/wallet_ | PUT | - | `Wallet` | `Wallet`
Delete a wallet | _/wallet_ | DELETE | - | `Wallet` | -
 |  |  |  | 
Get an asset of a wallet | _/wallet/{id}/{symbol}_ | GET | _id_ du wallet  _symbol_ de l'asset | - | `Asset`
Add an asset to a wallet | _/wallet/{id}_ | POST | _id_ du wallet | `Asset` | `Wallet` 
Remove an asset from a wallet | _/wallet/{id}/{symbol}_ | DELETE | _id_ du wallet  _symbol_ de l'asset | - | `Wallet`
 |  |  |  | 
Open a trade | _/wallet/{walletId}/{assetSymbol}_ | POST | _id_ du wallet  _symbol_ de l'asset | `Trade` | `Wallet`
Close a trade | _/wallet/{walletId}/{assetSymbol}_ | PUT | _id_ du wallet  _symbol_ de l'asset | `Trade` | `Wallet`


# Requirements

Java 8

Maven 3

# Development

### Database architecture
Le projet utilise deux bases de données MySQL.

###### crypto

![Crypto architecture](./.images/crypto.png)

###### wallet

![Wallet architecture](./.images/wallet.png)

### Structure du code

Le code est séparé en deux modules, correspondant chacun à un microservice.\
Chaque microservice repose sur une base de données.\
Lors d'une requête sur un microservice :

* Une couche **RESTController** est responsable de récupérer les requêtes reçues par l'API. Elle transmet la requête à la couche **Service**.
* La couche **Service** reçoit la requête et peut effectuer des traitements si besoin (aller récupérer le prix d'une _Crypto_ sur une API externe par exemple). Elle transmet finalement la requête au **DAO**.
* La couche **DAO** reçoit la requête et effectue le traitement adéquat en base.

 Dans ce projet, les couches **Service** et **DAO** sont des interfaces pouvant avoir plusieurs implémentations.\
 A ce jour, le **Service** n'a qu'une implémentation utilisant l'API externe _CoinMarketCap_ pour récupérer le prix d'une _Crypto_.\
 A ce jour, le **DAO** n'a qu'une implémentation utilisant _Hibernate_ pour accéder à la base de données _MySQL_.

### Gitlab CI/CD

Les tests sont automatisés à d'un Pipeline Gitlab CI/CD.\
Deux jobs sont utilisés pour chaque service, un pour le **build** et un pour le **test**.\
J'ai créé un [container Docker](https://hub.docker.com/u/vmzl) contenant les bases de données pre-remplies est utilisé par les jobs de **test**.

### Tâches

 - [x] Creation du module **Wallet-service**
 - [x] Ajout de la récupération du prix dans **crypto-service** via _CoinMarketCap_
 - [x] Ajout de l'implémentation autour des `Trade`
 - [x] Ajouter des tests
 - [x] Automatisation des tests avec GitLab CI/CD
 - [ ] Construction d'un front-end simple avec **Angular**
 - [ ] Ajouter PLUS de tests


